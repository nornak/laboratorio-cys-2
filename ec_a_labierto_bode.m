
function ec_a_labierto_bode

    %graphics_toolkit ("gnuplot");

	f1 = tf([13 0], [7 9]);

	f1

	bode(f1);

	%formato del grafico
	subplot(2,1,1)
	title('Diagrama de Bode de lazo abierto con H(s) = 13s/7s+9');	
	ylabel('Magnitud (dB)');
	grid;

	subplot(2,1,2)
	ylabel('Fase (grados)');
	xlabel('Frecuencia (rad/s)');	
	grid;

end		
