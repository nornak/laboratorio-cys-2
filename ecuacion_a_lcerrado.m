
function ecuacion_a_lcerrado

    %graphics_toolkit ("gnuplot");

	f1 = tf([13 0], [7 9]);

	f1

	f2 = feedback(f1, 1);

	f2

	step(f2,10);

	%formato del grafico
	title('Grafica de sistema de lazo cerrado con H(s) = 13s/7s+9');
	xlabel('Tiempo(Segundos)');	
	ylabel('Amplitud');
	grid;



end		
