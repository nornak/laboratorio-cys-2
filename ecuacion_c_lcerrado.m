
function ecuacion_c_lcerrado

    %graphics_toolkit ("gnuplot");

	f1 = tf([13 0 8 0], [4 3 0 5]);

	f1

	f2 = feedback(f1,1);

	f2

	step(f2,10);

	%formato del grafico
	title('Grafica de sistema de lazo cerrado con H(s) = 13s^3+8s/4s^3+3s^2+5');
	xlabel('Tiempo(Segundos)');	
	ylabel('Amplitud');
	grid;




end		
