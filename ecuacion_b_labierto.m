
function ecuacion_b_labierto

    %graphics_toolkit ("gnuplot");

	f1 = tf([5 11 5], [3 1 10]);

	f1

	step(f1,10);

	%formato del grafico
	title('Grafica de sistema de lazo abierto con H(s) = 5s^2+11s+5/3^2+s+10');
	xlabel('Tiempo(Segundos)');	
	ylabel('Amplitud');
	grid;


end		
