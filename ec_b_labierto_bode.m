
function ec_b_labierto_bode

    %graphics_toolkit ("gnuplot");

	f1 = tf([5 11 5], [3 1 10]);

	f1

	bode(f1);

	%formato del grafico
	subplot(2,1,1)
	title('Diagrama de Bode de lazo abierto con H(s) = 5s^2+11s+5/3^2+s+10');	
	ylabel('Magnitud (dB)');
	grid;

	subplot(2,1,2)
	ylabel('Fase (grados)');
	xlabel('Frecuencia (rad/s)');	
	grid;

end		
