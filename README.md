# Laboratorio Nº 2

## Diferencia Matlab

### step(num, dem)

No funciona `step` con dos argumentos. Para graficar primero se tiene que
obtener la función de transferencia, con la función `tf`. Esta función recibe
dos polinomio para el numerador o denominador.

Ejemplo:
    `tf([1 10], [10 5])`
    `tf(1 , [5 10 5])`

### cloop

La función `cloop()` de Matlab no esta en Octave. Hay que usar la función `tf`
para generar la función de transferencia. Tiene la forma `tf(num, dem)`, el
numerador y denominador de la función de transferencia. Con esto se tiene un
*lazo abierto* y para obtener un lazo cerrado se utiliza la función
`feedback(funcion)` o también `feedback(funcion, valor retroalimentacion)` y se
obtiene un *lazo cerrado*

- mas info en este [link](http://prof.usb.ve/lamanna/cursos/matlab.PDF)


