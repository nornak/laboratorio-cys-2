
function ec_b_lcerrado_bode

    %graphics_toolkit ("gnuplot");

	f1 = tf([5 11 5], [3 1 10]);

	f1
	
	f2 = feedback(f1,1);

	f2

	bode(f2);

	%formato del grafico
	subplot(2,1,1)
	title('Diagrama de Bode de lazo cerrado con H(s) = 5s^2+11s+5/3^2+s+10');	
	ylabel('Magnitud (dB)');
	grid;

	subplot(2,1,2)
	ylabel('Fase (grados)');
	xlabel('Frecuencia (rad/s)');	
	grid;



end		
