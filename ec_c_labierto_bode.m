
function ec_c_labierto_bode

    %graphics_toolkit ("gnuplot");

	f1 = tf([13 0 8 0], [4 3 0 5]);

	f1

	bode(f1);

	%formato del grafico
	subplot(2,1,1)
	title('Diagrama de Bode de lazo abierto con H(s) = 13s^3+8s/4s^3+3s^2+5');	
	ylabel('Magnitud (dB)');
	grid;

	subplot(2,1,2)
	ylabel('Fase (grados)');
	xlabel('Frecuencia (rad/s)');	
	grid;

end		
