
function ecuacion_c_labierto

    %graphics_toolkit ("gnuplot");

	f1 = tf([13 0 8 0], [4 3 0 5]);

	f1

	step(f1,10);

	%formato del grafico
	title('Grafica de sistema de lazo abierto con H(s) = 13s^3+8s/4s^3+3s^2+5');
	xlabel('Tiempo(Segundos)');	
	ylabel('Amplitud');
	grid;

end		
