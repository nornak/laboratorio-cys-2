
function ecuacion_b_lcerrado

    %graphics_toolkit ("gnuplot");

	f1 = tf([5 11 5], [3 1 10]);

	f1
	
	f2 = feedback(f1,1);

	f2

	step(f2,10);

	%formato del grafico
	title('Grafica de sistema de lazo cerrado con H(s) = 5s^2+11s+5/3^2+s+10');
	xlabel('Tiempo(Segundos)');	
	ylabel('Amplitud');
	grid;



end		
