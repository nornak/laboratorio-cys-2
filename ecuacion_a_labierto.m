
function ecuacion_a_labierto

    %graphics_toolkit ("gnuplot");

	f1 = tf([13 0], [7 9]);

	f1

	step(f1,10);

	%formato del grafico
	title('Grafica de sistema de lazo abierto con H(s) = 13s/7s+9');
	xlabel('Tiempo(Segundos)');	
	ylabel('Amplitud');
	grid;

end		
