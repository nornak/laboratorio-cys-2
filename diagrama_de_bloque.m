



function diagrama_de_bloque
    graphics_toolkit ("gnuplot");

    f1 = tf([0 0 1 0], [0 13 0 2]);
    f2 = tf([0 0 0 4], [0 0 4 9]);
    f3 = tf([0 0 6 8], [2 6 1 0]);
    f4 = tf([0 0 0 11], [0 0 2 9]);
    f5 = tf([0 0 7 3], [2 6 1 0]);
    f6 = tf([0 0 1 3], [0 1 6 10]);


    f1_2 = parallel(f1,f2);

    f4_5 = parallel(f4,f5);

    %f3_f = feedback(f3,1,"+");
    f3_f = feedback(f3,1);

    f3_f_4_5 = series(f3_f, f4_5);

    f3_f_4_5_6 = series(f3_f_4_5, f6);

    ftotal = parallel(f3_f_4_5_6, f1_2);

    ftotal


    step(ftotal);


endfunction
